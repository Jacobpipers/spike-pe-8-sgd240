# Spike Report

## SPIKE PE 8 - Movement Components

---

### Introduction

How players move is often left up to physics – especially in the case of vehicle-based games.
How does Unreal handle movement, and what options do we have to create our own styles of movement?

---

### Goals

* The Spike Report should answer each of the Gap questions

In a new “demo” project:

* Build a number of actors, driven by simple logic, which utilise ALL of the pre-existing Movement Components (C++ documentation found here).
  * InterpTo Movement Component 
  * Projectile Movement Component
  * Rotating Movement Component
  * Nav Movement Component
  * Pawn Movement Component
  * Character Movement Component
  * Arch Vis Char Movement Component
  * Floating Pawn Movement
  * Spectator Pawn Movement
  * Wheeled Vehicle Movement Component
  * Wheeled Vehicle Movement Component 4W
* Additionally, build a single UMovementComponent sub-class of your own that showcases some interesting, unique form of movement.

---

### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

---

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* [InterpTo Video](https://www.youtube.com/watch?v=6CyV3urf9lE)
* [InterpTo Documentation](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/Components/UInterpToMovementComponent/index.html?lang=ja)
* [Projectile Movement Comp](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/UProjectileMovementComponent/)
* [Rotating Movement](https://docs.unrealengine.com/latest/INT/Engine/Components/Movement/)
* [Rotating Movement Blueprint](https://wiki.unrealengine.com/Blueprint_Rotating_Movement_Component)
* [Rotating Movement Video](https://www.youtube.com/watch?v=gUk-BL-Iyug)
* [Nav Movement Component](https://docs.unrealengine.com/latest/INT/BlueprintAPI/AI/Components/NavMovement/)
* [Pawn Movement Component](https://docs.unrealengine.com/latest/INT/BlueprintAPI/Pawn/Components/PawnMovement/index.html)
* [Character Movement Components](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/UCharacterMovementComponent/index.html)
* [Arch Vis Char](https://docs.unrealengine.com/latest/INT/API/Plugins/ArchVisCharacter/UArchVisCharMovementComponent/index.html)
* [Floating Pawn](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/UFloatingPawnMovement/index.html)
* [Spectating Pawn Movement](https://docs.unrealengine.com/latest/INT/Engine/AI/BehaviorTrees/QuickStart/index.html)
* [Wheeled Vechile](https://docs.unrealengine.com/latest/INT/BlueprintAPI/Game/Components/WheeledVehicleMovement/index.html)
* [Wheeled Vechile Advanced](https://docs.unrealengine.com/latest/INT/Engine/Physics/Vehicles/VehicleUserGuide/1/index.html)
* [TeleportTo](https://docs.unrealengine.com/latest/INT/API/Runtime/Engine/GameFramework/AActor/TeleportTo/)
* Visual Studio
* Unreal Engine

---

### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

---

#### Implement Movement Compontents

##### InterpTo Movement Component

* Moves between multiple points.

  * In the InterpToActor add component and add control points in the constructer as seen below.

```c++
InterpComp->AddControlPointPosition(FVector::ZeroVector);
InterpComp->AddControlPointPosition(FVector(0, 300, 0));

InterpComp->Duration = 10.0f;
InterpComp->BehaviourType = EInterpToBehaviourType::PingPong;
```

---

---

##### Projectile Movement Component

* A premade component that makes a actor move like a bullet and allows it to bounce or not.

```c++
// Use a ProjectileMovementComponent to govern this projectile's movement
ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
ProjectileMovement->UpdatedComponent = CollisionComp;
ProjectileMovement->InitialSpeed = 3000.f;
ProjectileMovement->MaxSpeed = 3000.f;
ProjectileMovement->bRotationFollowsVelocity = true;
ProjectileMovement->bShouldBounce = true;

// Die after 3 seconds by default
InitialLifeSpan = 3.0f;
```

---

---

##### Rotating Movement Component

* Allows a Actor to rotate in Local or world space

* Add Rotation Component

  * Set Z Rotation Rate to 180

---

---

##### Nav Movement Component

* Allows an Actor to move on a Nav Mesh and to avoid objects.

  * Create Nav Mesh Bounds Volume
    * Set Box Size to cover level area that Ai will move

    * Set Up Ai Behaviour as seen in this [tutorial](https://docs.unrealengine.com/latest/INT/Engine/AI/BehaviorTrees/QuickStart/index.html) steps 1-15

---

---

##### Character Movement Component

* Adds a basic Humonid character movement with walking/running jumping etc.

---

---

##### Arch Vis Char Movement Component

* Is a Premade Character Movement with more smooth and slower movement. Good for visualisation and VR.

  * Make a AArchVisCharacter Charactor and inherit from AArchVisCharacter

---

---

##### Floating Pawn Movement

* Floating Pawn

  * Add Floating Movement Component

  * Map Input "Fly"

  * Add Movement input vector

```c++
FloatMove->AddInputVector(FVector(0, 0, 1000 * f));
```

---

---

##### Spectator Pawn Movement

Spectator Move Component is inherited from the Spectator class

* [Spectator](https://answers.unrealengine.com/questions/189367/startswitch-to-spectator-via-blueprints.html)

---

---

##### Wheeled Vehicle Movement Component

* Allows for wheels to be assigned to a mesh and be the driving force of the actor ussing torque.

* [Follow this Guide](https://docs.unrealengine.com/latest/INT/Engine/Physics/Vehicles/VehicleUserGuide/1/index.html)

---

---

##### Wheeled Vehicle Movement Component Advanced

* Advanced Vechile looks at the Suspension for more open vechiles as the defualt is good enough for cars with full bodies it can be problem for buggy like cars.

* [Advanced vechile doc](https://docs.unrealengine.com/latest/INT/Engine/Physics/Vehicles/DoubleWishboneVehicle/index.html)

---

---

##### Create Own Movement Component Blink

* Blink Movement

  * Create Class that inherits from UMovementComponent

  * In Tick of UMyMovementComponent want to send a trace forward from player and check if hit anything every time a bool blinkStart is true.

```c++
const FVector LineTraceStart = UpdatedComponent->GetComponentLocation();
const float TraceDist = Distance;

FCollisionQueryParams QueryParams;
QueryParams.AddIgnoredActor(UpdatedComponent->GetAttachmentRootActor());

QueryParams.TraceTag = UpdatedComponent->GetFName();


GetWorld()->DebugDrawTraceTag = QueryParams.TraceTag;

FHitResult Hit;

const auto b_is_hit = GetWorld()->LineTraceSingleByChannel(Hit, LineTraceStart, LineTraceStart + (UpdatedComponent->GetForwardVector() * TraceDist), ECC_Visibility, QueryParams);

```

* Before blink want object to be hidden

```c++
UpdatedComponent->SetHiddenInGame(true, false);
```

* After hidden move object to end of the trace or if it hits something move to whre the trace hits

```c++
UpdatedComponent->GetAttachmentRootActor()->TeleportTo(Hit.Location, UpdatedComponent->GetComponentRotation(), false, false);
```

* set blinkStart to false

* On Teleported set Componenet to be visisble again.

```C++
void UMyMovementComponent::OnTeleported()
{
    UpdatedComponent->SetHiddenInGame(false, false);
}
```

* Add New Blink Component to ThirdPersonCharacter or Any Pawn that want to use the blink movement component

  * Map Input

  * Set Blink start to true as seen below

```c++
Blink->blinkStart = true;
```

---

### What we found out

Knowledge: What are Movement Components responsibilities and capabilities?

Movement Components are like premade Movement Behaviours that can be easily used on diffrent actors/objects

### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

Vechile Advanced being imported from feture pack does not compile correctly in current project. Creating new Advanced Vechile C++ project does not appear to have this problem. Comenting out All Audio Component in TP_VechileAdvPawn appears to fix compile errors.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.