// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "Spike8GameMode.generated.h"

UCLASS(minimalapi)
class ASpike8GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASpike8GameMode();
};



