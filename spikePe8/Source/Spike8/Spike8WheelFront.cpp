// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Spike8.h"
#include "Spike8WheelFront.h"

USpike8WheelFront::USpike8WheelFront()
{
	ShapeRadius = 35.f;
	ShapeWidth = 10.0f;
	bAffectedByHandbrake = false;
	SteerAngle = 50.f;
}
