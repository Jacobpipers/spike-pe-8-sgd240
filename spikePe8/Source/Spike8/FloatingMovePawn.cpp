// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike8.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "FloatingMovePawn.h"

#include "Engine.h"

#define SDEBUG(V) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, V);

// Sets default values
AFloatingMovePawn::AFloatingMovePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	StaticMesh->SetupAttachment(RootComponent);

	// Create a spring arm component
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm0"));
	SpringArm->TargetOffset = FVector(0.f, 0.f, 200.f);
	SpringArm->SetRelativeRotation(FRotator(-15.f, 0.f, 0.f));
	SpringArm->SetupAttachment(StaticMesh);
	SpringArm->TargetArmLength = 600.0f;
	SpringArm->bEnableCameraRotationLag = true;
	SpringArm->CameraRotationLagSpeed = 7.f;
	SpringArm->bInheritPitch = false;
	SpringArm->bInheritRoll = false;

	// Create camera component 
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera0"));
	Camera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false;
	Camera->FieldOfView = 90.f;

	FloatMove = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("FloatingMove"));


}

// Called when the game starts or when spawned
void AFloatingMovePawn::BeginPlay()
{
	Super::BeginPlay();
	
}

void AFloatingMovePawn::Fly(float f)
{
	//SDEBUG("Fly")

	FloatMove->AddInputVector(FVector(0, 0, 1000 * f));

}

// Called every frame
void AFloatingMovePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AFloatingMovePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Vertical", this, &AFloatingMovePawn::Fly);
	
}

