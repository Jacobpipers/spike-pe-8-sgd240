// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VehicleWheel.h"
#include "Spike8WheelFront.generated.h"

UCLASS()
class USpike8WheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	USpike8WheelFront();
};



