// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike8.h"
#include "MyMovementComponent.h"
#include "Engine/World.h"

UMyMovementComponent::UMyMovementComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set Distance
	Distance = 100;
}

void UMyMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	
	// skip if don't want component updated when not rendered or updated component can't move
	if (ShouldSkipUpdate(DeltaTime))
	{
		return;
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!IsValid(UpdatedComponent))
	{
		return;
	}

	if(blinkStart)
	{
		// Simple blink - this will be a bool of Complexblink
		if (isComplex == false)
		{
			const FVector LineTraceStart = UpdatedComponent->GetComponentLocation();
			const float TraceDist = Distance;

			FCollisionQueryParams QueryParams;
			QueryParams.AddIgnoredActor(UpdatedComponent->GetAttachmentRootActor());

			QueryParams.TraceTag = UpdatedComponent->GetFName();


			GetWorld()->DebugDrawTraceTag = QueryParams.TraceTag;

			FHitResult Hit;

			const auto b_is_hit = GetWorld()->LineTraceSingleByChannel(Hit, LineTraceStart, LineTraceStart + (UpdatedComponent->GetForwardVector() * TraceDist), ECC_Visibility, QueryParams);

			UpdatedComponent->SetHiddenInGame(true, false);

			if (b_is_hit)
			{
				// set Actor Location at end of ray or at hit point.				
				UpdatedComponent->GetAttachmentRootActor()->TeleportTo(Hit.Location, UpdatedComponent->GetComponentRotation(), false, false);
			}
			else
			{
				UpdatedComponent->GetAttachmentRootActor()->TeleportTo(Hit.TraceEnd, UpdatedComponent->GetComponentRotation(), false, false);
			}
		}
		else
		{
			// Complex blink - TODO do further checks.
		}
		blinkStart = false;
	}
}


void UMyMovementComponent::OnTeleported()
{
	UpdatedComponent->SetHiddenInGame(false, false);
}
