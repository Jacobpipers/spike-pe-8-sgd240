// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/MovementComponent.h"
#include "MyMovementComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = Movement, meta = (BlueprintSpawnableComponent), HideCategories = (Velocity))
class SPIKE8_API UMyMovementComponent : public UMovementComponent
{
	GENERATED_UCLASS_BODY()


public:
	
	// How far player goes.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blink)
	float Distance;

	// How Long the blink last.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blink)
	float delay;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blink)
	bool isComplex;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blink)
	bool blinkStart;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = RotatingComponent)
	FVector PivotTranslation;

	//Begin UActorComponent Interface
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	//End UActorComponent Interface
	
	virtual void OnTeleported() override;
	
};
