// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike8.h"
#include "InterpToMeveActor.h"


// Sets default values
AInterpToMeveActor::AInterpToMeveActor()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InterpComp = CreateDefaultSubobject<UInterpToMovementComponent>(TEXT("InterpComp"));
	
	InterpComp->AddControlPointPosition(FVector::ZeroVector);
	InterpComp->AddControlPointPosition(FVector(0, 300, 0));

	InterpComp->Duration = 10.0f;
	InterpComp->BehaviourType = EInterpToBehaviourType::PingPong;

}

// Called when the game starts or when spawned
void AInterpToMeveActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInterpToMeveActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

// Called to bind functionality to input
void AInterpToMeveActor::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

