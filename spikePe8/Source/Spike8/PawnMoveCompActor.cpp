// Fill out your copyright notice in the Description page of Project Settings.

#include "Spike8.h"
#include "PawnMoveCompActor.h"

APawnMoveCompActor::APawnMoveCompActor()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//PawnMoveComp = CreateDefaultSubobject<UPawnMovementComponent>(TEXT("PawnMoveComp"));

}


// Called when the game starts or when spawned
void APawnMoveCompActor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APawnMoveCompActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	/*if (PawnMoveComp != nullptr)
	{
		PawnMoveComp->AddInputVector(5 * GetActorForwardVector());
		PawnMoveComp->MoveUpdatedComponent(FVector(10, 10, 0), FQuat(0, 0, 0, 0), false, 0, ETeleportType::TeleportPhysics);
	}*/
}

// Called to bind functionality to input
void APawnMoveCompActor::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}



