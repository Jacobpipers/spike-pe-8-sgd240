// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Spike8.h"
#include "Spike8WheelRear.h"

USpike8WheelRear::USpike8WheelRear()
{
	ShapeRadius = 35.f;
	ShapeWidth = 10.0f;
	bAffectedByHandbrake = true;
	SteerAngle = 0.f;
}
