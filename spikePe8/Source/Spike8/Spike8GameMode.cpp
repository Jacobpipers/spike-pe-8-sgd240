// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Spike8.h"
#include "Spike8GameMode.h"
#include "Spike8Pawn.h"
#include "Spike8Hud.h"

ASpike8GameMode::ASpike8GameMode()
{
	DefaultPawnClass = ASpike8Pawn::StaticClass();
	HUDClass = ASpike8Hud::StaticClass();
}
