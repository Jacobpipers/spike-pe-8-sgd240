// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyPawnBase.h"
#include "GameFramework/PawnMovementComponent.h"
#include "PawnMoveCompActor.generated.h"

/**
 * 
 */
UCLASS()
class SPIKE8_API APawnMoveCompActor : public AMyPawnBase
{
	GENERATED_BODY()

		/*UPROPERTY(Category = Movement, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UPawnMovementComponent * PawnMoveComp;*/

public:

	APawnMoveCompActor();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	
};
