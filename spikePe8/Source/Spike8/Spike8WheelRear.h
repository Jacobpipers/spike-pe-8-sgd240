// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VehicleWheel.h"
#include "Spike8WheelRear.generated.h"

UCLASS()
class USpike8WheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	USpike8WheelRear();
};



