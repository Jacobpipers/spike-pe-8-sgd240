// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MyPawnBase.h"
#include "Components/InterpToMovementComponent.h"

#include "InterpToMeveActor.generated.h"

UCLASS()
class SPIKE8_API AInterpToMeveActor : public AMyPawnBase
{
	GENERATED_BODY()

	UPROPERTY(Category = Movement, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UInterpToMovementComponent * InterpComp;

public:
	// Sets default values for this pawn's properties
	AInterpToMeveActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
